<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Sidenav -->
            <?php include('inc/sidenav.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="bar">
                    <div class="bar__left">
                        <div class="bar__heading">Dashboard</div>

                    </div>
                    <div class="bar__right">
                    </div>
                </div>
                <div class="wrap">
                    <div class="container">

                        <div class="heading">Payouts</div>
                        <div class="row">
                            <div class="col col-xs-12 col-md-6 col-lg-6 col-xl-4">
                                <a href="#" class="panel">
                                    <div class="panel__body">
                                        <h4 class="text-center">Today’s payouts</h4>
                                        <div class="text_value">
                                            <span class="text_value__elem">4,000<sup class="color_green">MHC</sup></span> /
                                            <span class="text_value__elem">224<sup class="color_green">EUR</sup></span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col col-xs-12 col-md-6 col-lg-6 col-xl-4">
                                <div class="panel">
                                    <div class="panel__body">
                                        <h4 class="text-center">Last 7 days</h4>
                                        <div class="text_value">
                                            <span class="text_value__elem">4,000<sup class="color_green">MHC</sup></span> /
                                            <span class="text_value__elem">224<sup class="color_green">EUR</sup></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-md-6 col-lg-6 col-xl-4">
                                <div class="panel">
                                    <div class="panel__body">
                                        <h4 class="text-center">Last 30 days</h4>
                                        <div class="text_value">
                                            <span class="text_value__elem">4,000<sup class="color_green">MHC</sup></span> /
                                            <span class="text_value__elem">224<sup class="color_green">EUR</sup></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="heading">Funds</div>
                        <div class="row mb_24">

                            <div class="col col-xs-12 col-md-6 col-lg-6 col-xl-4">
                                <div class="panel">
                                    <div class="panel__body">
                                        <h4 class="text-center">Delegated</h4>
                                        <div class="text_value">
                                            <span class="text_value__elem">4,000<sup class="color_green">MHC</sup></span> /
                                            <span class="text_value__elem">224<sup class="color_green">EUR</sup></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-md-6 col-lg-6 col-xl-4">
                                <div class="panel">
                                    <div class="panel__body">
                                        <h4 class="text-center">Pending</h4>
                                        <div class="text_value">
                                            <span class="text_value__elem">4,000<sup class="color_green">MHC</sup></span> /
                                            <span class="text_value__elem">224<sup>EUR</sup></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-md-6 col-lg-6 col-xl-4">
                                <div class="panel">
                                    <div class="panel__body">
                                        <h4 class="text-center">From the beginning</h4>
                                        <div class="text_value">
                                            <span class="text_value__elem">4,000<sup>MHC</sup></span> /
                                            <span class="text_value__elem">224<sup class="color_green">EUR</sup></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-md-6 col-lg-6 col-xl-4">
                                <div class="panel">
                                    <div class="panel__body">
                                        <h4 class="text-center">To be delegated</h4>
                                        <div class="text_value">
                                            <span class="text_value__elem">4,000 <sup class="color_green">MHC</sup></span>

                                            <span class="text_value__elem">
                                            <a href="#" class="btn btn_red"><strong>Delegate now</strong></a>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-md-6 col-lg-6 col-xl-4">
                                <div class="panel">
                                    <div class="panel__body">
                                        <h4 class="text-center">Top up</h4>
                                        <div class="text_value">
                                            <span class="text_value__elem"><a href="#" class="btn btn_blue"><strong>Add funds</strong></a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col col-lg-6">
                                <div class="panel">
                                    <div class="panel__heading">
                                        <div class="panel__title">Statistics</div>
                                        <div class="btn_group btn_switch">
                                            <a href="#" class="btn btn_border btn-sm active">Today</a>
                                            <a href="#" class="btn btn_border btn-sm">Yesterday</a>
                                            <a href="#" class="btn btn_border btn-sm">Last week</a>
                                        </div>
                                    </div>
                                    <div class="panel__body">
                                        <img src="images/chart_01.png" class="img-fluid" alt="chart">
                                    </div>
                                </div>
                            </div>

                            <div class="col col-lg-6">
                                <div class="panel">
                                    <div class="panel__heading">
                                        <div class="panel__title">Transaction history</div>
                                    </div>
                                    <div class="panel__body">
                                        <div class="table_responsive">
                                            <table class="table table_mobile">
                                                <tr>
                                                    <th>date</th>
                                                    <th>time</th>
                                                    <th>in MHC</th>
                                                    <th>in EUR</th>
                                                    <th>status</th>
                                                </tr>
                                                <tr>
                                                    <td data-label="date"><strong>Summary for: February 18th 2019 - March 20th 2019</strong></td>
                                                    <td data-label="time" class="text-nowrap"><strong>All</strong></td>
                                                    <td data-label="in MHC" class="text-nowrap"><strong>100,000</strong></td>
                                                    <td data-label="in EUR" class="text-nowrap"><strong>5,700</strong></td>
                                                    <td data-label="status"></td>
                                                </tr>
                                                <tr>
                                                    <td data-label="date" class="text-nowrap">20.03.2019</td>
                                                    <td data-label="time" class="text-nowrap">17:01</td>
                                                    <td data-label="in MHC" class="text-nowrap">1,000</td>
                                                    <td data-label="in EUR" class="text-nowrap">57</td>
                                                    <td data-label="status">
                                                        <span class="label_base label_green">Payout</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td data-label="date" class="text-nowrap">20.03.2019</td>
                                                    <td data-label="time" class="text-nowrap">17:01</td>
                                                    <td data-label="in MHC" class="text-nowrap">1,000</td>
                                                    <td data-label="in EUR" class="text-nowrap">57</td>
                                                    <td data-label="status">
                                                        <span class="label_base label_yellow">Payout</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td data-label="date" class="text-nowrap">20.03.2019</td>
                                                    <td data-label="time" class="text-nowrap">17:01</td>
                                                    <td data-label="in MHC" class="text-nowrap">1,000</td>
                                                    <td data-label="in EUR" class="text-nowrap">57</td>
                                                    <td data-label="status">
                                                        <span class="label_base label_yellow">Payout</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td data-label="date" class="text-nowrap">20.03.2019</td>
                                                    <td data-label="time" class="text-nowrap">17:01</td>
                                                    <td data-label="in MHC" class="text-nowrap">1,000</td>
                                                    <td data-label="in EUR" class="text-nowrap">57</td>
                                                    <td data-label="status">
                                                        <span class="label_base label_yellow">Payout</span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="text-center">
                                            <a href="#" class="btn btn_xs">Show more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </section>


        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
