<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Sidenav -->
            <?php include('inc/sidenav.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="bar">
                    <div class="bar__left">
                        <div class="bar__heading">Statistics</div>
                        <div class="sort">
                            <a class="active" href="#">Last 30 day</a>
                            <a href="#">Last 7 day</a>
                            <a href="#">All Time</a>
                        </div>
                    </div>
                    <div class="bar__right">
                        <div class="period">
                            <div class="period__selected">
                                <i class="fas fa-calendar-alt"></i>
                                <span>February 20th 2019 - March 22nd 2019</span>
                                <i class="fas fa-angle-down"></i>
                            </div>
                            <ul class="period__dropdown">
                                <li><a href="#" data-value="Today">Today</a></li>
                                <li><a href="#" data-value="Yesterday">Yesterday</a></li>
                                <li class="active"><a href="#" data-value="Last 7 days">Last 7 days</a></li>
                                <li><a href="#" data-value="This Month">This Month</a></li>
                                <li><a href="#" data-value="Last Month">Last Month</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col col-xs-12">
                                <div class="panel">
                                    <div class="panel__body">
                                        <img src="images/chart_02.png" class="img-fluid" alt="chart">
                                    </div>
                                </div>
                            </div>
                            <div class="col col-xs-12">
                                <div class="panel">
                                    <div class="panel__heading">
                                        <div class="panel__title">Payout history</div>
                                    </div>
                                    <div class="panel__body">
                                        <div class="table_responsive">
                                            <table class="table table_mobile">
                                                <tr>
                                                    <th>date</th>
                                                    <th>time</th>
                                                    <th>in MHC</th>
                                                    <th>in EUR</th>
                                                    <th>status</th>
                                                </tr>
                                                <tr>
                                                    <td data-label="date"><strong>Summary for: February 18th 2019 - March 20th 2019</strong></td>
                                                    <td data-label="time" class="text-nowrap"><strong>All</strong></td>
                                                    <td data-label="in MHC" class="text-nowrap"><strong>100,000</strong></td>
                                                    <td data-label="in EUR" class="text-nowrap"><strong>5,700</strong></td>
                                                    <td data-label="status"></td>
                                                </tr>
                                                <tr>
                                                    <td data-label="date" class="text-nowrap">20.03.2019</td>
                                                    <td data-label="time" class="text-nowrap">17:01</td>
                                                    <td data-label="in MHC" class="text-nowrap">1,000</td>
                                                    <td data-label="in EUR" class="text-nowrap">57</td>
                                                    <td data-label="status">
                                                        <span class="label_base label_green">Payout</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td data-label="date" class="text-nowrap">20.03.2019</td>
                                                    <td data-label="time" class="text-nowrap">17:01</td>
                                                    <td data-label="in MHC" class="text-nowrap">1,000</td>
                                                    <td data-label="in EUR" class="text-nowrap">57</td>
                                                    <td data-label="status">
                                                        <span class="label_base label_yellow">Payout</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td data-label="date" class="text-nowrap">20.03.2019</td>
                                                    <td data-label="time" class="text-nowrap">17:01</td>
                                                    <td data-label="in MHC" class="text-nowrap">1,000</td>
                                                    <td data-label="in EUR" class="text-nowrap">57</td>
                                                    <td data-label="status">
                                                        <span class="label_base label_yellow">Payout</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td data-label="date" class="text-nowrap">20.03.2019</td>
                                                    <td data-label="time" class="text-nowrap">17:01</td>
                                                    <td data-label="in MHC" class="text-nowrap">1,000</td>
                                                    <td data-label="in EUR" class="text-nowrap">57</td>
                                                    <td data-label="status">
                                                        <span class="label_base label_yellow">Payout</span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>


        </div>


        <!-- Feedback -->
        <div class="hide">
            <div class="modal" id="feedback">
                <div class="modal__heading">Feedback</div>
                <div class="modal__body">
                    <form class="form">
                        <div class="form_group">
                            <div class="form_label_line">Email:</div>
                            <input class="form_control" type="text" name="email" placeholder="Email" value="">
                        </div>
                        <div class="form_group">
                            <div class="form_label_line">Your question:</div>
                            <textarea class="form_control" name="message" rows="6" placeholder=""></textarea>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn_md">Send feedback</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
