<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Sidenav -->
            <?php include('inc/sidenav.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="bar">
                    <div class="bar__left">
                        <div class="bar__heading">Account settings</div>
                        <div class="bar__nav">
                            <a class="active" href="#">Personal</a>
                            <a href="#">Change Password</a>
                        </div>
                    </div>
                    <div class="bar__right">

                    </div>
                </div>
                <div class="wrap">
                    <div class="container">

                        <div class="account">
                            <div class="account__heading">Account settings</div>
                            <div class="account__body">
                                <form class="form">
                                    <div class="row form_group">
                                        <div class="col col-xs-12 col-md-4 col-gutter-lr">
                                            <div class="form_label_line">Email:</div>
                                        </div>
                                        <div class="col col-xs-12 col-md-8 col-gutter-lr">
                                            <input class="form_control" type="text" name="email" placeholder="" value="dis@itdv.pro">
                                        </div>
                                    </div>
                                    <div class="row form_group">
                                        <div class="col col-xs-12 col-md-4 col-gutter-lr">
                                            <div class="form_label_line">Phone number:</div>
                                        </div>
                                        <div class="col col-xs-12 col-md-8 col-gutter-lr">
                                            <input class="form_control" type="text" name="phone" placeholder="" value="dis@itdv.pro">
                                        </div>
                                    </div>
                                    <div class="row form_group">
                                        <div class="col col-xs-6 col-sm-4 col-md-4 col-gutter-lr">
                                            <div class="form_label_line">MFA:</div>
                                        </div>
                                        <div class="col col-xs-6 col-sm-4 col-md-8 col-gutter-lr">
                                            <label class="form_checkbox">
                                                <input type="checkbox" name="check" checked>
                                                <span><i class="fas fa-check"></i></span>
                                            </label>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </section>


        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
