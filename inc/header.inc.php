<header class="header">
    <div class="container">
        <div class="header__row">
            <a class="header__logo" href="/">
                <img src="img/logo.svg" class="img-fluid" alt="">
            </a>
            <div class="header__elements">

                <span class="header_exchange hide-xs-only hide-sm-only">MHC / EUR: <strong>0,045</strong></span>

                <div class="header_currency">
                    <div class="header_currency__active">
                        <span class="header_currency__label">Currency</span>
                        <span class="header_currency__value">EUR</span>
                        <i class="fas fa-angle-down"></i>
                    </div>
                    <ul class="header_currency__dropdown">
                        <li class="active"><a href="#" data-value="EUR">EUR</a></li>
                        <li><a href="#" data-value="USD">USD</a></li>
                        <li><a href="#" data-value="BTC">BTC</a></li>
                        <li><a href="#" data-value="LTC">LTC</a></li>
                    </ul>
                </div>

                <span class="header_summary hide-xs-only">40,000 EUR</span>

                <div class="header_lng">
                    <div class="header_lng__active">
                        <i>
                            <img src="img/icon__lng_en.svg" class="img-fluid" alt="">
                        </i>
                    </div>
                    <ul class="header_lng__dropdown">
                        <li>
                            <a href="#">
                                <i><img src="img/icon__lng_en.svg" class="img-fluid" alt=""></i>
                                <span>EN</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i><img src="img/icon__lng_ru.png" class="img-fluid" alt=""></i>
                                <span>RU</span>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="header_account">
                    <div class="header_account__button">
                        <i>
                            <svg class="ico-svg" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__user" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </div>
                    <ul class="header_account__dropdown">
                        <li><a href="#">Account</a></li>
                        <li><a href="#">Settings</a></li>
                        <li><a href="#">Exit</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>