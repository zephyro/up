<nav class="sidenav">

    <ul>
        <li>
            <a href="#">
                <i><img src="img/sidenav__icon_01.svg" class="img-fluid" alt=""></i>
                <span>Dashboard</span>
            </a>
        </li>
        <li>
            <a href="#">
                <i><img src="img/sidenav__icon_02.svg" class="img-fluid" alt=""></i>
                <span>Statistics</span>
            </a>
        </li>
        <li class="active">
            <a href="#">
                <i><img src="img/sidenav__icon_03.svg" class="img-fluid" alt=""></i>
                <span>Add funds</span>
            </a>
        </li>
        <li>
            <a href="#">
                <i><img src="img/sidenav__icon_04.svg" class="img-fluid" alt=""></i>
                <span>Withdrawal</span>
            </a>
        </li>
    </ul>

    <ul>
        <li>
            <a href="#">
                <i><img src="img/sidenav__icon_05.svg" class="img-fluid" alt=""></i>
                <span>Settings</span>
            </a>
        </li>
        <li>
            <a href="#">
                <i><img src="img/sidenav__icon_06.svg" class="img-fluid" alt=""></i>
                <span>F.A.Q.</span>
            </a>
        </li>
        <li>
            <a href="#feedback" class="btn_modal">
                <i><img src="img/sidenav__icon_07.svg" class="img-fluid" alt=""></i>
                <span>Feedback</span>
            </a>
        </li>
    </ul>

</nav>