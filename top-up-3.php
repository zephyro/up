<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Sidenav -->
            <?php include('inc/sidenav.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="bar">
                    <div class="bar__left">
                        <div class="bar__heading">Top up</div>
                    </div>
                    <div class="bar__right">

                    </div>
                </div>
                <div class="wrap">
                    <div class="container">

                        <ul class="steps">
                            <li class="steps_good">Enter amount</li>
                            <li class="steps_good">Choose provider</li>
                            <li class="steps_good">Success</li>
                        </ul>

                        <div class="step">
                            <div class="step__heading">Step 3: Success</div>
                            <div class="step__body">
                                <p>Your transaction has been successfully done.</p>
                                <p>Total amount: 27 EUR</p>
                            </div>
                            <div class="step__bottom">
                                <div class="step__bottom_center">
                                    <button class="btn btn_blue btn_md">Back to Dashboard</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>


        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
