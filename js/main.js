// SVG IE11 support
svg4everybody();


$(".btn_modal").fancybox({
    'padding'    : 0
});


// Currency
(function() {

    $('.header_currency__active').on('click touchstart', function(e){
        e.preventDefault();


        if($(this).closest('.header_currency').hasClass('open')){
            $(this).closest('.header_currency').toggleClass('open');
        }
        else {
            $('.header_currency').removeClass('open');
            $(this).closest('.header_currency').toggleClass('open');
        }

    });

    $('.header_currency__dropdown a').on('click touchstart', function(e){

        var txt = $(this).text();

        $(this).closest('.header_currency__dropdown').find('li').removeClass('active');
        $(this).closest('li').addClass('active');

        $(this).closest('.header_currency').find('.header_currency__value').text(txt);
        $(this).closest('.header_currency').removeClass('open');

    });

}());


// Lang
(function() {

    $('.header_lng__active').on('click touchstart', function(e){
        e.preventDefault();

        if($(this).closest('.header_lng').hasClass('open')){
            $(this).closest('.header_lng').toggleClass('open');
        }
        else {
            $('.header_lng').removeClass('open');
            $(this).closest('.header_lng').toggleClass('open');
        }

    });

    $('.header_lng__dropdown a').on('click touchstart', function(e){

        var txt = $(this).find('i').html();

        $(this).closest('.header_lng').find('.header_lng__active i').html(txt);
        $(this).closest('.header_lng').removeClass('open');

    });

}());

// Account
(function() {

    $('.header_account__button').on('click touchstart', function(e){
        e.preventDefault();

        if($(this).closest('.header_account').hasClass('open')){
            $(this).closest('.header_account').toggleClass('open');
        }
        else {
            $('.header_account').removeClass('open');
            $(this).closest('.header_account').toggleClass('open');
        }

    });

    $('.header_account__dropdown a').on('click touchstart', function(e){
        $(this).closest('.header_account').removeClass('open');

    });

}());


// Currency
(function() {

    $('.switch__active').on('click touchstart', function(e){
        e.preventDefault();


        if($(this).closest('.switch').hasClass('open')){
            $(this).closest('.switch').toggleClass('open');
        }
        else {
            $('.switch').removeClass('open');
            $(this).closest('.switch').toggleClass('open');
        }

    });

    $('.switch__dropdown a').on('click touchstart', function(e){

        var txt = $(this).text();

        $(this).closest('.switch').find('.switch__value').text(txt);
        $(this).closest('.switch').removeClass('open');

    });

}());


// Period
(function() {

    $('.period__selected').on('click touchstart', function(e){
        e.preventDefault();


        if($(this).closest('.period').hasClass('open')){
            $(this).closest('.period').toggleClass('open');
        }
        else {
            $('.period').removeClass('open');
            $(this).closest('.period').toggleClass('open');
        }

    });

    $('.period__dropdown a').on('click touchstart', function(e){

        var txt = $(this).text();

        $(this).closest('.period__dropdown').find('li').removeClass('active');
        $(this).closest('li').addClass('active');
        $(this).closest('.period').removeClass('open');

    });

}());



// Hide dropdown

$('body').click(function (event) {

    if ($(event.target).closest(".header_lng").length === 0) {
        $(".header_lng").removeClass('open');
    }

    if ($(event.target).closest(".header_account").length === 0) {
        $(".header_account").removeClass('open');
    }

    if ($(event.target).closest(".header_currency").length === 0) {
        $(".header_currency").removeClass('open');
    }
});


// Sort
(function() {

    $('.sort a').on('click touchstart', function(e){
        e.preventDefault();

        $(this).closest('.sort').find('a').removeClass('active');
        $(this).addClass('active');

    });
}());



(function() {

    $('.btn_switch a').on('click touchstart', function(e){
        e.preventDefault();

        $(this).closest('.btn_switch').find('a').removeClass('active');
        $(this).addClass('active');

    });
}());