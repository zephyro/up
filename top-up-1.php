<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Sidenav -->
            <?php include('inc/sidenav.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="bar">
                    <div class="bar__left">
                        <div class="bar__heading">Top up</div>
                    </div>
                    <div class="bar__right">

                    </div>
                </div>
                <div class="wrap">
                    <div class="container">

                        <ul class="steps">
                            <li class="steps_good">Enter amount</li>
                            <li>Choose provider</li>
                            <li>Success</li>
                        </ul>

                        <div class="step">
                            <div class="step__heading">Step 1: Enter amount</div>
                            <div class="step__body">
                                <div class="step__form">
                                    <div class="row">
                                        <div class="col col-xs-12 col-md-6">
                                            <div class="form_group">
                                                <div class="form_label">Amount:</div>
                                                <div class="input_wrap">
                                                    <input class="form_control" type="text" name="val1" placeholder="" value="600">
                                                    <div class="input_switch">
                                                        <div class="switch">
                                                            <div class="switch__active">
                                                                <span class="switch__label">Currency</span>
                                                                <span class="switch__value">EUR</span>
                                                                <i class="fas fa-angle-down"></i>
                                                            </div>
                                                            <ul class="switch__dropdown">
                                                                <li><a href="#" data-value="EUR">EUR</a></li>
                                                                <li><a href="#" data-value="USD">USD</a></li>
                                                                <li><a href="#" data-value="BTC">BTC</a></li>
                                                                <li><a href="#" data-value="LTC">LTC</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text_info">Recomended</div>
                                            <ul class="btn_list">
                                                <li><button type="button" class="btn btn_border_blue">30 EUR</button></li>
                                                <li><button type="button" class="btn btn_border_blue">60 EUR</button></li>
                                                <li><button type="button" class="btn btn_border_blue">120 EUR</button></li>
                                            </ul>
                                        </div>
                                        <div class="col col-xs-12 col-md-6">
                                            <div class="form_group">
                                                <div class="form_label">Amount in MHC:</div>
                                                <input class="form_control" type="text" name="val1" placeholder="" value="600">
                                            </div>
                                            <div class="text_info">Recomended</div>
                                            <ul class="btn_list">
                                                <li><button type="button" class="btn btn_border_blue">30 EUR</button></li>
                                                <li><button type="button" class="btn btn_border_blue">60 EUR</button></li>
                                                <li><button type="button" class="btn btn_border_blue">120 EUR</button></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="step__bottom">
                                <div class="step__bottom_col">
                                    <div class="step__total">Amount: 27 EUR</div>
                                </div>
                                <div class="step__bottom_col">
                                    <button type="button" class="btn btn_md btn_blue">Next</button>
                                </div>
                            </div>
                        </div>

                        <div class="panel">
                            <div class="panel__heading">
                                <div class="panel__title">Top up history</div>
                            </div>
                            <div class="panel__body">
                                <div class="table_responsive">
                                    <table class="table table_mobile">
                                        <tr>
                                            <th>date</th>
                                            <th>time</th>
                                            <th>Amount</th>
                                            <th>Type of payment</th>
                                            <th>status</th>
                                            <th>Details</th>
                                        </tr>
                                        <tr>
                                            <td data-label="date" class="text-nowrap">20.03.2019</td>
                                            <td data-label="time" class="text-nowrap">17:01</td>
                                            <td data-label="Amount" class="text-nowrap">0,01 BTC</td>
                                            <td data-label="status" class="text-nowrap">BTC transaction</td>
                                            <td data-label="status">
                                                <span class="label_base label_yellow">Pending</span>
                                            </td>
                                            <td data-label="Details">Some additional text</td>
                                        </tr>
                                        <tr>
                                            <td data-label="date" class="text-nowrap">20.03.2019</td>
                                            <td data-label="time" class="text-nowrap">17:01</td>
                                            <td data-label="Amount" class="text-nowrap">0,01 BTC</td>
                                            <td data-label="status" class="text-nowrap">BTC transaction</td>
                                            <td data-label="status">
                                                <span class="label_base label_yellow">Pending</span>
                                            </td>
                                            <td data-label="Details">Some additional text</td>
                                        </tr>
                                        <tr>
                                            <td data-label="date" class="text-nowrap">20.03.2019</td>
                                            <td data-label="time" class="text-nowrap">17:01</td>
                                            <td data-label="Amount" class="text-nowrap">0,01 BTC</td>
                                            <td data-label="status" class="text-nowrap">BTC transaction</td>
                                            <td data-label="status">
                                                <span class="label_base label_yellow">Pending</span>
                                            </td>
                                            <td data-label="Details">Some additional text</td>
                                        </tr>
                                        <tr>
                                            <td data-label="date" class="text-nowrap">20.03.2019</td>
                                            <td data-label="time" class="text-nowrap">17:01</td>
                                            <td data-label="Amount" class="text-nowrap">0,01 BTC</td>
                                            <td data-label="status" class="text-nowrap">BTC transaction</td>
                                            <td data-label="status">
                                                <span class="label_base label_yellow">Pending</span>
                                            </td>
                                            <td data-label="Details">Some additional text</td>
                                        </tr>
                                        <tr>
                                            <td data-label="date" class="text-nowrap">20.03.2019</td>
                                            <td data-label="time" class="text-nowrap">17:01</td>
                                            <td data-label="Amount" class="text-nowrap">0,01 BTC</td>
                                            <td data-label="status" class="text-nowrap">BTC transaction</td>
                                            <td data-label="status">
                                                <span class="label_base label_yellow">Pending</span>
                                            </td>
                                            <td data-label="Details">Some additional text</td>
                                        </tr>
                                        <tr>
                                            <td data-label="date" class="text-nowrap">20.03.2019</td>
                                            <td data-label="time" class="text-nowrap">17:01</td>
                                            <td data-label="Amount" class="text-nowrap">0,01 BTC</td>
                                            <td data-label="status" class="text-nowrap">BTC transaction</td>
                                            <td data-label="status">
                                                <span class="label_base label_yellow">Pending</span>
                                            </td>
                                            <td data-label="Details">Some additional text</td>
                                        </tr>
                                        <tr>
                                            <td data-label="date" class="text-nowrap">20.03.2019</td>
                                            <td data-label="time" class="text-nowrap">17:01</td>
                                            <td data-label="Amount" class="text-nowrap">0,01 BTC</td>
                                            <td data-label="status" class="text-nowrap">BTC transaction</td>
                                            <td data-label="status">
                                                <span class="label_base label_yellow">Pending</span>
                                            </td>
                                            <td data-label="Details">Some additional text</td>
                                        </tr>
                                        <tr>
                                            <td data-label="date" class="text-nowrap">20.03.2019</td>
                                            <td data-label="time" class="text-nowrap">17:01</td>
                                            <td data-label="Amount" class="text-nowrap">0,01 BTC</td>
                                            <td data-label="status" class="text-nowrap">BTC transaction</td>
                                            <td data-label="status">
                                                <span class="label_base label_yellow">Pending</span>
                                            </td>
                                            <td data-label="Details">Some additional text</td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>


        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
