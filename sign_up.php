<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page page_no_sidenav">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="bar">
                    <div class="bar__left">
                        <div class="bar__heading">Create a free account</div>
                    </div>
                    <div class="bar__right">

                    </div>
                </div>
                <div class="wrap">
                    <div class="container">

                        <div class="account">
                            <div class="account__heading">Create a free account</div>
                            <div class="account__body">
                                <div class="form_group">
                                    <button type="submit" class="btn btn_rose btn_md btn_long"><i class="fab fa-google"></i><span>Sign up width Google</span></button>
                                </div>
                                <div class="form_group">
                                    <button type="submit" class="btn btn_blue btn_md btn_long"><i class="fab fa-facebook-f"></i><span>Sign up width Facebook</span></button>
                                </div>
                                <form class="form">
                                    <div class="form_group">
                                        <div class="form_label_line">Email:</div>
                                        <input class="form_control" type="text" name="email" placeholder="Email" value="">
                                    </div>
                                    <div class="row form_group">
                                        <div class="col col-gutter-lr">
                                            <a href="#">Sign in</a>
                                        </div>
                                        <div class="col col-gutter-lr text-right">
                                            <a href="#">Forgot password?</a>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn_md btn_long">Create Account</button>

                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </section>


        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
