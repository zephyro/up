<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Sidenav -->
            <?php include('inc/sidenav.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="bar">
                    <div class="bar__left">
                        <div class="bar__heading">Top up</div>
                    </div>
                    <div class="bar__right">

                    </div>
                </div>
                <div class="wrap">
                    <div class="container">

                        <ul class="steps">
                            <li class="steps_good">Enter amount</li>
                            <li class="steps_good">Choose provider</li>
                            <li>Success</li>
                        </ul>

                        <div class="step">
                            <div class="step__heading">Step 2: Choose pay provider</div>
                            <div class="step__body">
                                <div class="step_pay">
                                    <span class="step_pay__amount">Amount: 27 EUR</span>
                                    <span class="step_pay__type">Type: MASTERCARD</span>
                                </div>

                                <ul class="pay_list">
                                    <li>
                                        <label class="pay_item">
                                            <input type="radio" name="pay_type">
                                            <div class="pay_item__elem">
                                                <div class="pay_item__icon">
                                                    <i><img src="img/pay__btc.png" alt="btc"></i>
                                                    <span>BTC transaction</span>
                                                </div>
                                            </div>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="pay_item">
                                            <input type="radio" name="pay_type">
                                            <div class="pay_item__elem">
                                                <div class="pay_item__icon">
                                                    <i><img src="img/pay__mc.png" alt="btc"></i>
                                                    <span></span>
                                                </div>
                                            </div>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="pay_item">
                                            <input type="radio" name="pay_type">
                                            <div class="pay_item__elem">
                                                <div class="pay_item__icon">
                                                    <i><img src="img/pay__visa.png" alt="btc"></i>
                                                    <span></span>
                                                </div>
                                            </div>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="pay_item">
                                            <input type="radio" name="pay_type">
                                            <div class="pay_item__elem">
                                                <div class="pay_item__icon">
                                                    <i><img src="img/pay__quiwi.png" alt="btc"></i>
                                                    <span></span>
                                                </div>
                                            </div>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="pay_item">
                                            <input type="radio" name="pay_type">
                                            <div class="pay_item__elem">
                                                <div class="pay_item__icon">
                                                    <i><img src="img/pay__btc.png" alt="btc"></i>
                                                    <span></span>
                                                </div>
                                            </div>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="pay_item">
                                            <input type="radio" name="pay_type">
                                            <div class="pay_item__elem">
                                                <div class="pay_item__icon">
                                                    <i><img src="img/pay__btc.png" alt="btc"></i>
                                                    <span>PAYEER</span>
                                                </div>
                                            </div>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                            <div class="step__bottom">
                                <div class="step__bottom_col">
                                    <button type="button" class="btn btn_md btn_gray">Back</button>
                                </div>
                                <div class="step__bottom_col">
                                    <button type="button" class="btn btn_md btn_blue">Next</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>


        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
